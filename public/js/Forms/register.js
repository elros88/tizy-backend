//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
    el: '#vueregister',
    data: {
      globalurl:'../api/v1/absencetype',
      fd:
      {
        id:'',
        dni:'',
        name:'',
        gender:'',
        phone:'',
        email:'',
        password1:'',
        password2:'',
      

      },
      ff:
      {
        save:false,
        edit:true,
      }    
      
    },
    methods: {
      guardar: function (event) 
      {
        swal({
                  title: "Confirmación de Registro",
                  text: "Estas seguro de registrarte",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonText: 'Aceptar',
                  cancelButtonText: 'Cancelar'
              }).then(function () {
  
                    if(mod.fd.password1 === mod.fd.password2)
                    {
                        var arr = {
                            //DATOS DE FORMULARIO
                              dni:mod.fd.dni,
                              name:mod.fd.name,
                              gender:mod.fd.gender,
                              phone:mod.fd.phone,
                              email:mod.fd.email,
                              password:mod.fd.password1,
                                          
                          };
                    }
                    else{
                       
                        swal('Contrasenas no coinciden');
                        return false;
                    }
                 
                    //VUE POST
                  Vue.http.post('http://api.'+$urldomain+'/user',arr).then(response => {
                      // Si todo sale correcto              
                      swal('Guardado Correctamente','','success');
                      $('#ModalEdicion').modal('hide');
                      mod.cleanform();
  
                      }, response => {
                        //Si no sale correcto
                              swal(response.body,'','error')
  
                          });
                          
                      })
                      //FIN VUE POST
             
         
           
  
      },
      editar: function (event) 
      {
          var items = [mod.fd.Name,mod.fd.Description];
          var strings = ['Nombre', 'Descripcion'];
          var checked = CheckNulls(items, strings);
          if(checked !== 0){
         swal({
                  title: "Confirmación de editado",
                  text: "Estas seguro de editar este elemento",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonText: 'Aceptar',
                  cancelButtonText: 'Cancelar'
              }).then(function () {
  
  
                  var arr = {
                      //DATOS DE FORMULARIO
                      name:mod.fd.Name,
                      description:mod.fd.Description
                                        
                    };
                    //VUE POST
                  Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                      // Si todo sale correcto              
                      swal('Modificada Correctamente','','success');
                      $('#ModalEdicion').modal('hide');
                      mod.cleanform();
  
                      }, response => {
                        //Si no sale correcto
                              swal(response.body,'','error')
  
                          });
                          
                      })
                      //FIN VUE POST
          }
      },
      borrar: function (event)
      {
          swal({
                  title: "Confirmar Eliminación",
                  text: "Estas seguro de eliminar este elemento",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonText: 'Aceptar',
                  cancelButtonText: 'Cancelar'
              }).then(function () {
  
  
                  var arr = {name:mod.fd.Name};
                  Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                      // success callback
                      swal('Eliminado Correctamente','','success');
                  $('#ModalEdicion').modal('hide');
                  mod.cleanform();
  
  
              }, response => {
                      swal('Ocurrio un error al eliminar','','error');
                      // error callback
                  });
              })
      },
      cleanform: function()
      {
        mod.fd.Idn='';
        mod.fd.Name='';
        mod.fd.Description='';
        $('#ModalEdicion').modal('hide');
        var table = $('#TablaMaster').dataTable();
        //RECARGA LOS DATOS DE LA TABLA
       table.fnReloadAjax();
       
      },
     
       openmodaltonew:function()
          {
            mod.cleanform();
            $('#ModalEdicion').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            mod.ff.save = true;
            mod.ff.edit = false;
          },
          openmodaltoedit:function()
          {
             $('#ModalEdicion').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            mod.ff.save = false;
            mod.ff.edit = true;
          }
    }
   
  });