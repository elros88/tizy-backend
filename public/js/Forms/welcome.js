CargarTabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#welcome',
  data: {
    globalurl:'http://api.tizy.kstesting.com.ve',
    fd:
    {
      Idn:'',
      Name:'',
      Fecha:'',
      Token:$tokenuser,
      Email:'',
      Balance:'454',
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
      getbalance:function(event)
      {
        Vue.http.get(mod.globalurl+"/wallet/user/"+mod.fd.Token).then(response => {
            // success callback
           mod.fd.Balance = response.data.data.balance;
    }, response => {
            swal('Ocurrio un error al eliminar','','error');
            // error callback
        });
      },
    guardar: function (event) 
    {
        var items = [mod.fd.Name,mod.fd.Description];
        var strings = ['Nombre', 'Descripcion'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    name:mod.fd.Name,
                    description:mod.fd.Description                    
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')

                        });
                        
                    })
                    //FIN VUE POST
           
       
         }

    },
    editar: function (event) 
    {
        var items = [mod.fd.Name,mod.fd.Description];
        var strings = ['Nombre', 'Descripcion'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    name:mod.fd.Name,
                    description:mod.fd.Description
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')

                        });
                        
                    })
                    //FIN VUE POST
        }
    },
    borrar: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.fd.Name};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },
    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.Name='';
      mod.fd.Description='';
      $('#ModalEdicion').modal('hide');
      var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

 //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#table_id").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "450px",
        "scrollX":        "1000px",
       
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [

            { data : 'dni'},
             { data : 'name'},
                 { data : 'phone'},
                  { data : 'email'}
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: 'http://api.tizy.kstesting.com.ve/user',
            dataSrc : 'data'
        }

    });

            $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Name = table.row( this ).data().name;
        mod.fd.Description = table.row( this ).data().description;    
      
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });
          }

          mod.getbalance();