<?php
    
    namespace App\Core\Enums;
    
    class DaysEnum {
        public static $sunday    = 1;
        public static $monday    = 2;
        public static $tuesday   = 3;
        public static $wednesday = 4;
        public static $thursday  = 5;
        public static $friday    = 6;
        public static $saturday  = 7;
        
    }