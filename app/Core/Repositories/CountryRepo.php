<?php

    namespace App\Core\Repositories;

    use App\Core\Entities\Country;
    
    class CountryRepo {
      
        public function all() {

            $countries = Country::orderBy('iso', 'asc')->get();

            return $countries;
        }

        public function allServer() {

            $countries = Country::all();

            return $countries;
        }

        public function find($id) {

            $countries = Country::find($id);

            return $countries;
        }

        public function update($id, $data) {

            $countries = Country::find($id);
            $countries->fill($data);
            $countries->save();

            return $countries;
        }

        public function update_dni($id, $data) {

            $country      = Country::find($id);
            $country->dni = $data;
            $country->save();

            return $country;
        }

        public function dni($country_id) {

            $country = Country::find($country_id);

            return $country;
        }
    }
