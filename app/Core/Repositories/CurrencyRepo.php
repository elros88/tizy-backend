<?php
    
    namespace App\Core\Repositories;
    
    use App\Core\Entities\Currency;
    
    class CurrencyRepo {
        
        public function all() {
            $currencies = Currency::orderBy('iso', 'asc')->get();
            
            return $currencies;
        }
        
        public function find($iso) {
            $currency = Currency::where('iso', $iso)->first();
            
            return $currency;
        }
    }
