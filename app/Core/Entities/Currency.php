<?php
    
    namespace App\Core\Entities;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Currency extends Model {
        protected $table = 'currencies';
        
        protected $connection = 'Totus_Food';
        
        protected $primaryKey = 'iso';
        
        public $timestamps = false;
        
        public $incrementing = false;
        
        protected $fillable = [
            'iso',
            'name',
            'symbol',
            'thousand_separator',
            'decimal_separator',
            'decimals',
        ];
        
        public function getFullNameAttribute() {
            return $this->iso . ' - ' . $this->name;
        }
        
        public function scopeTitle($query, $value) {
            return $query->where('iso', $value)->value('symbol');
        }
        
    }
