<?php
    use Illuminate\Support\Facades\Gate;
    
    if (!function_exists('_asset')) {
        function _asset($path) {
            if (!is_null(request()->server('HTTP_X_FORWARDED_PROTO'))) {
                return asset($path, true);
            } else {
                return asset($path);
            }
        }
    }
    
    if (!function_exists('_route')) {
        function _route($name, $parameters = [], $absolute = true) {
            if (!is_null(request()->server('HTTP_X_FORWARDED_PROTO'))) {
                return str_replace('http://', 'https://',route($name, $parameters, $absolute));
            } else {
                return route($name, $parameters, $absolute);
            }
        }
    }
    
    if (!function_exists('_url')) {
        function _url($path, $parameters = []) {
            if (!is_null(request()->server('HTTP_X_FORWARDED_PROTO'))) {
                return str_replace('http://', 'https://', url($path, $parameters, true));
            } else {
                return url($path, $parameters);
            }
        }
    }
    
    if (!function_exists('cURL')) {
        function cURL($url, $method = 'GET', array $o = [], $api = true) {
            $options['http']['method'] = $method;
            
            if (isset($o['header'])) {
                $options['http']['header'] = $o['header'];
            }
            
            if (isset($o['data'])) {
                $options['http']['content'] = @http_build_query($o['data']);
            }
            
            $context = @stream_context_create($options);
            $content = @file_get_contents($url, false, $context);
            
            if ($api) {
                return json_decode($content);
            } else {
                return $content;
            }
        }
    }
    