<?php
    
    return [
        'Passengers Title' => [
            'id'         => 'passenger.title',
            'icon'       => 'person',
            'url'        => 'back.passenger.index',
            'url_params' => [],
            'permission' => \App\Security\Enums\Permissions::$login,
            'type_user'  => null,
            'slug'       => null,
            'trans'      => __('Pasajeros'),
            'subMenu'    => [],
        ],
    ];
