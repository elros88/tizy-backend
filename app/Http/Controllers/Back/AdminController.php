<?php
    
    namespace App\Http\Controllers\Back;

    use Illuminate\Routing\Controller as BaseController;
    
    class AdminController extends BaseController {
        public function users(){
            return view('back.Admin.Users');
        }
    }
