<?php
    
    namespace App\Http\Controllers\Back;
    use Illuminate\Http\Request;
    use Illuminate\Routing\Controller as BaseController;
    
    class UserController extends BaseController {
        public function create(Request $request){
            return view('home');
        }

        public function edit(Request $request){
            return view('home');
        }

        public function register(){
            return view('back.auth.register');
        }
        public function qr($token){
            return view('back.qr');
        }
    }
