<?php
    
    namespace App\Http\Controllers\Web;
    
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Gate;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Mail;
    use App\Components\Helper;
    use App\Components\UUID;
    use App\Security\Entities\Role;
    use App\Security\Enums\Permissions;
    use App\Security\Enums\Roles;
    use App\Security\Repositories\RoleRepo;
    use App\Users\Entities\User;
    use App\UserType\Enums\TypeUser;
  
    class AuthController extends Controller {
        
        public function authenticate(Request $request, RoleRepo $roleRepo) {
        
        }
        
        public function login() {
            
            return view('auth.login');
        }
        
        public function logout() {
            
            Auth::logout();
            
            $response = [
                'status'  => 'OK',
                'title'   => __('¡Hasta pronto!'),
                'message' => __('Gracias por preferirnos') . '.',
            ];
            
            return redirect()->route('admin.auth.login')->with($response);
        }
        
        public static function register(Request $request) {
        
        
        }
        
    }
