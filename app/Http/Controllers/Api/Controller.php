<?php
    
    namespace App\Http\Controllers\Api;
    
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    
    class Controller extends BaseController {
        use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
        
        public function index() {
            return [
                'Api'     => 'Delivery',
                'versión' => '1.0',
                'Date'   => date('d-m-Y'),
            ];
        }
    }
