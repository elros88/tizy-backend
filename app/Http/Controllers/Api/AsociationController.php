<?php

namespace App\Http\Controllers\Api;

use App\Components\UUID;
use App\Asociation\Repositories\AsociationRepo;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class AsociationController extends BaseController
{

    private $AsociationRepo;

    public function __construct(AsociationRepo $AsociationRepo)
    {

        $this->AsociationRepo = $AsociationRepo;
    }

    public function index()
    {

        try {
            $users = $this->AsociationRepo->all();

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $users,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            log::error($ex);
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function get($token)
    {

        try {
            $user = $this->AsociationRepo->findByToken($token);

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'dni' => 'required',
            'gender' => 'required',
            'password' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'rol' => 'required'
        ], $this->custom_message());

        if ($validator->fails()) {

            $response = [
                'status' => 'FAILED',
                'code' => 400,
                'message' => __('Parametros incorrectos'),
                'data' => $validator->errors()->getMessages(),
            ];

            return response()->json($response, 400);
        }

        try {
            $request['token'] = UUID::v4();
            $request['active'] = 1;
            $request['status'] = 1;
            $user = $this->AsociationRepo->store($request->all());
            $response = [
                'status' => 'OK',
                'code' => '200',
                'message' => __('Usuario Creado Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function update($token, Request $request)
    {

        try {
            $data = [];
            if($request->has('name')){
                $data['name'] = $request->get('name');
            }
            if($request->has('phone')){
                $data['phone'] = $request->get('phone');
            }
            if($request->has('password')){
                $data['password'] = $request->get('password');
            }
            if($request->has('email')){
                $data['email'] = $request->get('email');
            }
            if($request->has('dni')){
                $data['dni'] = $request->get('dni');
            }
            if($request->has('gender')){
                $data['gender'] = $request->get('gender');
            }
            if($request->has('rol')){
                $data['rol'] = $request->get('rol');
            }

            $user = $this->AsociationRepo->find_token($token);
            $user = $this->AsociationRepo->update($user, $request->all());

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Actualizado Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }
    }

    public function delete($token)
    {

        try {
            $user = $this->AsociationRepo->find_token($token);
            $user = $this->AsociationRepo->delete($user, ['active' => false]);
            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Eliminado Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function custom_message()
    {

        return [
            'name.required' => __('El nombre es requerido') . '.',
            'username.required' => __('El nombre de usuario es requerido') . '.',
            'token.required' => __('El token es requerido') . '.',
        ];

    }

}
