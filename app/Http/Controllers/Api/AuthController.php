<?php
    
    namespace App\Http\Controllers\Api;

    use App\Security\Entities\Role;
    use App\Security\Enums\Roles;
    use App\Users\Entities\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Validator;

    class AuthController extends Controller {
        
        public function driver(Request $request) {
            $validator = Validator::make($request->all(), [
                'dni' => 'required',
                'password' => 'required',
            ], $this->custom_message());

            if ($validator->fails()) {

                $response = [
                    'status' => 'FAILED',
                    'code' => 400,
                    'message' => __('Parametros incorrectos'),
                    'data' => $validator->errors()->getMessages(),
                ];

                return response()->json($response, 400);
            }
            try {
                $username = $request->input('dni');
                $password = $request->input('password');
                $credentials = [
                    'dni' => $username,
                    'password' => $password
                ];
                if (Auth::attempt($credentials)) {
                    $user = User::find(Auth::id());
                    if($user->rol !== Roles::$driver){
                        session()->forget('permissions');
                        Auth::logout();
                        $response = [
                            'status' => 'FAILED',
                            'title' => __('¡Vaya! ¡Algo salió mal!'),
                            'message' => __('No Tienes Permisos para acceder'),
                        ];
                        return response()->json($response,401);
                    }

                    $response = [
                        'status' => 'OK',
                        'title' => '¡Bienvenido!',
                        'message' => 'Bienvenido',
                        'data' => [
                            'sessionId' => session()->getId(),
                            'dataUser' => $user
                        ]
                    ];
                    $rol = Role::find($user->rol);
                    $permissions = $rol->permissions;
                    $permissionsArray = [];
                    foreach ($permissions as $permission){
                        $permissionsArray[$permission->id] = $permission->id;
                    }
                    session()->put('permissions',$permissionsArray);
                    return response()->json($response,200);
                } else {
                    $response = [
                        'status' => 'FAILED',
                        'title' => __('¡Vaya! ¡Algo salió mal!'),
                        'message' => __('Credenciales incorrectas'),
                    ];
                    return response()->json($response,401);
                }

            } catch (\Exception $exception) {
                session()->forget('permissions');
                Auth::logout();
                $response = [
                    'status' => 'FAILED',
                    'title' => __('¡Vaya! ¡Algo salió mal!'),
                    'message' => $exception->getMessage(),
                ];
            }
            return response()->json($response,500);
        }

        public function passenger(Request $request) {
            $validator = Validator::make($request->all(), [
                'dni' => 'required',
                'password' => 'required',
            ], $this->custom_message());

            if ($validator->fails()) {

                $response = [
                    'status' => 'FAILED',
                    'code' => 400,
                    'message' => __('Parametros incorrectos'),
                    'data' => $validator->errors()->getMessages(),
                ];

                return response()->json($response, 400);
            }
            try {
                $username = $request->input('dni');
                $password = $request->input('password');
                $credentials = [
                    'dni' => $username,
                    'password' => $password
                ];
                if (Auth::attempt($credentials)) {
                    $user = User::find(Auth::id());
                    if($user->rol !== Roles::$passenger){
                        session()->forget('permissions');
                        Auth::logout();
                        $response = [
                            'status' => 'FAILED',
                            'title' => __('¡Vaya! ¡Algo salió mal!'),
                            'message' => __('No Tienes Permisos para acceder'),
                        ];
                        return response()->json($response,401);
                    }

                    $response = [
                        'status' => 'OK',
                        'title' => '¡Bienvenido!',
                        'message' => 'Bienvenido',
                        'data' => [
                            'sessionId' => session()->getId(),
                            'dataUser' => $user
                        ]
                    ];
                    $rol = Role::find($user->rol);
                    $permissions = $rol->permissions;
                    $permissionsArray = [];
                    foreach ($permissions as $permission){
                        $permissionsArray[$permission->id] = $permission->id;
                    }
                    session()->put('permissions',$permissionsArray);
                    return response()->json($response,200);
                } else {
                    $response = [
                        'status' => 'FAILED',
                        'title' => __('¡Vaya! ¡Algo salió mal!'),
                        'message' => __('Credenciales incorrectas'),
                    ];
                    return response()->json($response,401);
                }

            } catch (\Exception $exception) {
                session()->forget('permissions');
                Auth::logout();
                $response = [
                    'status' => 'FAILED',
                    'title' => __('¡Vaya! ¡Algo salió mal!'),
                    'message' => $exception->getMessage(),
                ];
            }
            return response()->json($response,500);
        }

        public function logout() {

        }

        public function custom_message()
        {
            return [
                'dni.required' => __('La Cedula es requerida'),
                'username.required' => __('El nombre de usuario es requerido'),
                'email.required' => __('El Correo Electrónico es requerido'),
                'password.required' => __('La Contraseña es requerida')
            ];
        }
    }
