<?php

namespace App\Users\Repositories;

use Mockery\Matcher\Type;
use App\Users\Entities\User;
use App\UserType\Enums\TypeUser;


class UserRepo
{
    public function all()
    {

        $users = User::all();

        return $users;
    }

    public function passengers()
    {

        $users = User::where(['active' => 1, 'rol' => 4])->get();

        return $users;
    }



    public function find($id)
    {

        $user = User::find($id);

        return $user;
    }

    public function find_token($token)
    {

        $user = User::Token($token);

        return $user;
    }

    public function store($data)
    {

        $user = new User();
        $user->fill($data);
        $user->save();

        return $user;
    }

    public function update($user, $data)
    {

        $user->fill($data);
        $user->save();

        return $user;
    }

    public function delete($user, $data)
    {

        $user->fill($data);
        $user->save();

        return $user;
    }
}
