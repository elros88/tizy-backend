<?php

namespace App\Users\Entities;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'id',
        'dni',
        'name',
        'gender',
        'token',
        'phone',
        'email',
        'rol',
        'password',
        'active',
        'status',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function scopeToken($query, $value)
    {
        return $query->where('token', $value)->first();
    }

}
