<?php
    
    namespace App\UserType\Entities;
    
    use Illuminate\Database\Eloquent\Model;
    
    

    class UserType extends Model {
        

        protected $table = 'user_type';

        public $timestamps = false;

        protected $primaryKey = 'id';
        
        /**
         * Fillable fields
         *
         * @var array
         */
        protected $fillable = [
            'id',
            'title',
        ];
        

        public function getDetailsAttribute($details) {
            return json_decode($details);
        }

        public function setDetailsAttribute($details) {
            $this->attributes['details'] = json_encode($details);
        }
    }
