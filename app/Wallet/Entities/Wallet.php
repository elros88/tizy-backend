<?php
    
    namespace App\Wallet\Entities;
    
    use App\Users\Entities\User;
    use Illuminate\Database\Eloquent\Model;
    
    class Wallet extends Model {

        protected $table      = 'wallet';
        protected $primaryKey = 'id';
        
        protected $fillable = [
            'id',         
            'token',
            'iduser',
            'balance',
            'active',
            
        ];
        public function scopeToken($query, $value) {
            return $query->where('token', $value)->first();
        }

    }
