<?php
    
    namespace App\Security\Entities;
    
    use Illuminate\Database\Eloquent\Model;
    use App\Users\Entities\User;
    
    class Role extends Model {
        protected $table = 'roles';
        
        protected $primaryKey = 'id';
        
        public $timestamps = false;
        
        public $fillable = [
            'id',
            'name',
        ];
        
        public function permissions() {
            return $this->belongsToMany(Permission::class);
        }
        
        public function users() {
            return $this->belongsToMany(User::class);
        }
    }
