<?php

namespace App\Security\Enums;


class Roles
{

    public static $root = 1;
    public static $admin = 2;
    public static $driver = 3;
    public static $passenger = 4;
}
