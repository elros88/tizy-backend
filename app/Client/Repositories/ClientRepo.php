<?php

    namespace App\Client\Repositories;
    
    use App\Client\Entities\Client;
    use App\ClientCompany\Entities\ClientCompany;

    class ClientRepo {
    
        public function all() {

            $Client = Client::all();

            foreach ($Client as $Client)
            {
                $ClientCompany = ClientCompany::find($Client->clientcompany);
                $Client->clientcompany = $ClientCompany;
            }

            return $Client;
        }
  
        public function find($id) {
    
            $Client = Client::find($id);

            return $Client;
        }

        public function findLike($clientcompany,$query) {
            $Client = Client::where('clientcompany',$clientcompany)->where('name', 'like', '%' . $query . '%')->get();
            return $Client;
        }
        
        public function findByToken($token) {
            $Client = Client::Token($token);
            if(isset($Client->clientcompany))
            {
            $ClientCompany = ClientCompany::find($Client->clientcompany);
            $Client->clientcompany = $ClientCompany;            
            }
            return $Client;
        }      

        public function store($data) {
    
            $Client = new Client();
            $Client->fill($data);
            $Client->save();

            return $Client;
        }
        
        public function update($Client, $data) {
    
            $Client->fill($data);
            $Client->save();

            return $Client;
        }
        public function delete($Client,$data) {

            $Client->active = false;
            $Client->save();

            return $Client;
        }
    }
