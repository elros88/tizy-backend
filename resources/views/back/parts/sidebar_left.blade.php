<aside id="leftsidebar" class="sidebar">
    <div class="user-info">
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{\Auth::user()->name}}</div>
            <div class="email">{{\Auth::user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="{{route('back.logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Menu -->
    <div class="menu">
        @if(Auth::user()->rol === 1 || Auth::user()->rol === 2)
                <ul class="list">
                    <li class="header">MENU ADMINISTRATIVO</li>
                    <li>
                        <a href="/users">
                            <i class="material-icons">account_circle</i>
                            <span>Usuarios</span>
                        </a>
                    </li>
                    <li>
                        <a href="/users">
                            <i class="material-icons">account_circle</i>
                            <span>Conductores</span>
                        </a>
                    </li>

                   <li>
                        <a href="/recoverpassword">
                            <i class="material-icons">https</i>
                            <span>Recuperar Contraseña</span>
                        </a>
                    </li>

					<li>
                        <a href="/drivers">
                            <i class="material-icons">directions_car</i>
                            <span>Listado De Conductores</span>
                        </a>
                    </li>
                    <li class="active">
                    </li>
              </ul>
           @endif
            <!-- #Menu -->
            @if (Auth::user()->rol === 3 )
            <ul class="list">
                    <li class="header">MENU DE CONDUCTOR</li>
                    <li>
                        <a href="/profile">
                            <i class="material-icons">account_circle</i>
                            <span>Perfil de Usuario</span>
                        </a>
                    </li>

                   <li>
                        <a href="/transactions">
                            <i class="material-icons">https</i>
                            <span>Transacciones</span>
                        </a>
                    </li>

					<li>
                        <a href="/recharge">
                            <i class="material-icons">directions_car</i>
                            <span>Retirar Saldo</span>
                        </a>
                    </li>
                    <li class="active">
                    </li>
              </ul>
            @endif
           @if (Auth::user()->rol === 4 )


             <ul class="list">
                    <li class="header">MENU DE PASAJERO</li>
                    <li>
                        <a href="/profile">
                            <i class="material-icons">account_circle</i>
                            <span>Perfil de Usuario</span>
                        </a>
                    </li>

                   <li>
                        <a href="/transactions">
                            <i class="material-icons">https</i>
                            <span>Transacciones</span>
                        </a>
                    </li>

					<li>
                        <a href="/recharge">
                            <i class="material-icons">directions_car</i>
                            <span>Recargar Saldo</span>
                        </a>
                    </li>
                    <li class="active">
                    </li>
              </ul>

 @endif


             </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
       
    </div>
    <!-- #Footer -->
</aside>