<?php
    
    use Illuminate\Database\Seeder;
    
    class RolesTableSeeder extends Seeder {
        
        public function run() {
            \App\Security\Entities\Role::create([
                'name' => 'Root',
            ]);
            
            \App\Security\Entities\Role::create([
                'name' => 'Admin',
            ]);
            
            \App\Security\Entities\Role::create([
                'name' => 'Driver',
            ]);
            
            \App\Security\Entities\Role::create([
                'name' => 'Passenger',
            ]);
        }
    }
