<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
   
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->char('iso', 3);
            $table->string('name', 30);
            $table->string('symbol', 15);
            $table->char('thousand_separator', 1);
            $table->char('decimal_separator', 1);
            $table->integer('decimals');
            $table->primary('iso');
        });
    }

    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
