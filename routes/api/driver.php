<?php
    
    use Illuminate\Http\Request;

    Route::group([
        'middleware' => [
            'api',
        ],
        'prefix'     => 'driver',
    ], function () {
        
        Route::get('/', [
            'as'   => 'api.driver',
            'uses' => 'Api\DriverController@index',
        ]);
        
        Route::post('/', [
            'as'   => 'api.driver.store',
            'uses' => 'Api\DriverController@store',
        ]);
        
        Route::get('/{token}', [
            'as'   => 'api.driver.get',
            'uses' => 'Api\DriverController@get',
        ]);
        
        Route::put('/{token}', [
            'as'   => 'api.driver.update',
            'uses' => 'Api\DriverController@update',
        ]);
    
        Route::delete('/{token}', [
            'as'   => 'api.driver.delete',
            'uses' => 'Api\DriverController@delete',
        ]);
        
    });
