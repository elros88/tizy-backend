<?php
    
    use Illuminate\Http\Request;

    Route::group([
        'middleware' => [
            'api',
        ],
        'prefix'     => 'asociation',
    ], function () {
        
        Route::get('/', [
            'as'   => 'api.asociation',
            'uses' => 'Api\AsociationController@index',
        ]);
        
        Route::post('/', [
            'as'   => 'api.asociation.store',
            'uses' => 'Api\AsociationController@store',
        ]);
        
        Route::get('/{token}', [
            'as'   => 'api.asociation.get',
            'uses' => 'Api\AsociationController@get',
        ]);
        
        Route::put('/{token}', [
            'as'   => 'api.asociation.update',
            'uses' => 'Api\AsociationController@update',
        ]);
    
        Route::delete('/{token}', [
            'as'   => 'api.asociation.delete',
            'uses' => 'Api\AsociationController@delete',
        ]);
        
    });
