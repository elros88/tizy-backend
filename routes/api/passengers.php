<?php
Route::group([
    'middleware' => [
        'api',
    ],
    'prefix' => '/passenger',
], function () {

    Route::get('', [
        'middleware' => ['permission:' . \App\Security\Enums\Permissions::$login,],
        'as' => 'api.passenger.index',
        'uses' => 'Api\UserController@passengers',
    ]);

});

