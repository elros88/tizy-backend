<?php


    Route::group([
        'middleware' => [
            'api',
        ],
        'prefix'     => 'auth',
    ], function () {

        Route::post('/driver/login', [
            'as'   => 'api.driver.login',
            'uses' => 'Api\AuthController@driver',
        ]);

        Route::post('/passenger/login', [
            'as'   => 'api.passenger.login',
            'uses' => 'Api\AuthController@passenger',
        ]);

        Route::get('/logout', [
            'as'   => 'api.logout',
            'uses' => 'Api\AuthController@logout',
        ]);
    });
