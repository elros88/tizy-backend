<?php
    
    /**
     * Auth routes
     */
    \Route::group(['middleware' => ['guest']], function () {
        
        // Show login
        Route::get('login', [
            'as'   => 'login',
            'uses' => 'Back\AuthController@login',
        ]);
        // Login users
        Route::post('login', [
            'as'   => 'back.login',
            'uses' => 'Back\AuthController@authenticate',
        ]);
    
        Route::get('register', [
            'as'   => 'back.register',
            'uses' => 'Back\AuthController@register',
        ]);
        
        Route::post('register', [
            'as'   => 'back.register.post',
            'uses' => 'Back\AuthController@register_post',
        ]);
        
    });
    
    \Route::group(['middleware' => ['auth']], function () {
        
        //        Logout users
        Route::get('logout', [
            'as'   => 'back.logout',
            'uses' => 'Back\AuthController@logout',
        ]);

    });
    