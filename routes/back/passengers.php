<?php
Route::group([
    'middleware' => [
        'auth',
    ],
    'prefix' => '/passenger',
], function () {

    Route::get('', [
        'middleware' => ['permission:' . \App\Security\Enums\Permissions::$login,],
        'as' => 'back.passenger.index',
        'uses' => 'Back\PassengerController@index',
    ]);

});

