<?php
Route::group([
    'middleware' => [
        'auth',
    ],
    'prefix' => '/users',
], function () {

    Route::get('', [
        'middleware' => ['permission:' . \App\Security\Enums\Permissions::$login,],
        'as' => 'back.admin.users',
        'uses' => 'Back\AdminController@users',
    ]);

});